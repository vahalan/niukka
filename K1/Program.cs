﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace niukka
{
    class Program
    {
        static void Main(string[] args)
        {
            // Tee 'niukka'-peli. Peli kysyy käyttäjältä luvun ja 
            // lisää siihen yhden. Sen jälkeen se tulostaa käyttäjälle
            // tämän luvun ja viestin "hävisit niukasti!"

            // Lisää kokonaislukumuuttuja käyttäjän arvausta varten
            int luku;

            // Kysy käyttäjältä lukua
            Console.Write("Anna luku ");
            luku = int.Parse(Console.ReadLine());

            // Kasvata sitä yhdellä
            luku++;

            // Tulosta viesti käyttäjälle
            Console.WriteLine("Koneen arvaus on " + luku + ". Hävisit niukasti!");

        }
    }
}
